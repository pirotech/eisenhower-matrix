import React, { useCallback, useEffect } from 'react';
import { DndProvider } from 'react-dnd';
import { HTML5Backend } from 'react-dnd-html5-backend';
import { Cell } from './cell';
import { CellsTypes, Store, Task } from './types.ts';
import { useLocalStorageState } from './hooks/useLocalStorageState.ts';
import { TasksList } from './tasks-list';
import { ShareButton } from './share-button';
import { STORE_PARAM_NAME } from './constants.ts';
import styles from './app.module.scss';

const App: React.FC = () => {
  const storeFromParamsString = new URLSearchParams(location.search).get(
    STORE_PARAM_NAME,
  );
  const storeFromParams = storeFromParamsString
    ? JSON.parse(storeFromParamsString)
    : undefined;

  const [store, setStore] = useLocalStorageState<Store>(
    'store',
    storeFromParams ?? {
      'urgent-important': [],
      'not-urgent-important': [],
      'urgent-not-important': [],
      'not-urgent-not-important': [],
    },
  );

  // fix tasks without id
  useEffect(() => {
    if (store) {
      let storeChanged = false;
      const storeWithIds = Object.entries(store)
        .map(([name, tasks]) => {
          const tasksWithIds = tasks.map((item) => {
            if (!item.id) {
              storeChanged = true;
              return { ...item, id: item.label };
            }
            return item;
          });
          return [name, tasksWithIds] as [string, Task[]];
        })
        .reduce((sum, [name, tasks]) => ({ ...sum, [name]: tasks }), {});
      if (storeChanged) {
        setStore(storeWithIds as Store);
      }
    }
  }, [store]);

  useEffect(() => {
    if (storeFromParams) {
      location.replace('/');
    }
  }, []);

  const handleRemove = useCallback(
    (task: Task) => {
      setStore((old) => {
        // object to array
        const asArray = Object.entries({ ...old });
        // remove old task
        const newStoreAsArray = asArray.map(([name, tasks]) => {
          return [name, tasks.filter((item) => item.id !== task.id)] as [
            CellsTypes,
            Task[],
          ];
        });
        // convert to Store
        return newStoreAsArray.reduce(
          (sum, [name, tasks]) => ({ ...sum, [name]: tasks }),
          {},
        ) as Store;
      });
    },
    [setStore],
  );

  const handleAdd = useCallback(
    (cellType: CellsTypes, task: Task) => {
      setStore((old) =>
        old ? { ...old, [cellType]: [...old[cellType], task] } : undefined,
      );
    },
    [setStore],
  );

  const handleMove = useCallback(
    (cellType: CellsTypes, task: Task) => {
      const isMoveToSamePlace = JSON.stringify(store?.[cellType]).includes(
        task.id,
      );
      if (!isMoveToSamePlace) {
        handleRemove(task);
        handleAdd(cellType, task);
      }
    },
    [handleRemove, handleAdd, store],
  );

  const handleEdit = useCallback(
    (task: Task) => {
      if (store) {
        setStore(
          Object.entries(store)
            .map(([name, tasks]) => {
              return [
                name,
                tasks.map((item) => (item.id === task.id ? task : item)),
              ] as [string, Task[]];
            })
            .reduce(
              (sum, [name, tasks]) => ({ ...sum, [name]: tasks }),
              {},
            ) as Store,
        );
      }
    },
    [store],
  );

  return (
    <DndProvider backend={HTML5Backend}>
      <div className={styles.wrapper}>
        <div className={styles.row}>
          <div className={styles.emptyCorner}>
            <ShareButton store={store} />
          </div>
          <div className={styles.headerCell_horizontal}>срочно</div>
          <div className={styles.headerCell_horizontal}>не срочно</div>
        </div>
        <div className={styles.row}>
          <div className={styles.headerCell_vertical}>
            <span>важно</span>
          </div>
          <Cell
            className={styles.cell_urgentImportant}
            name={'urgent-important'}
            hint={
              <>
                Квадрат кризиса
                <br />
                <br />
                Пример: сходить к зубному, вызвать сантехника, купить билеты в
                театр
              </>
            }
            onAdd={handleAdd}
          >
            <TasksList
              tasks={store?.['urgent-important']}
              onMove={handleMove}
              onRemove={handleRemove}
              onEdit={handleEdit}
            />
          </Cell>
          <Cell
            className={styles.cell_notUrgentImportant}
            name={'not-urgent-important'}
            hint={
              <>
                Стратегический квадрат
                <br />
                <br />
                Долгосрочные цели, задачи для личного роста, перспектив. Таких
                задач должно быть большинство
                <br />
                Пример: пройти курс, сходить в тренажёрный зал, настроить
                передачи на велосипеде
              </>
            }
            onAdd={handleAdd}
          >
            <TasksList
              tasks={store?.['not-urgent-important']}
              onMove={handleMove}
              onRemove={handleRemove}
              onEdit={handleEdit}
            />
          </Cell>
        </div>
        <div className={styles.row}>
          <div className={styles.headerCell_vertical}>
            <span>не&#160;важно</span>
          </div>
          <Cell
            className={styles.cell_urgentNotImportant}
            name={'urgent-not-important'}
            hint={
              <>
                Квадрат суеты
                <br />
                <br />
                Задачи, которые можно делегировать или автоматизировать,
                пожиратели времени
                <br />
                Пример: поставить латку на колесо, помыть посуду
              </>
            }
            onAdd={handleAdd}
          >
            <TasksList
              tasks={store?.['urgent-not-important']}
              onMove={handleMove}
              onRemove={handleRemove}
              onEdit={handleEdit}
            />
          </Cell>
          <Cell
            className={styles.cell_notUrgentNotImportant}
            name={'not-urgent-not-important'}
            hint={
              <>
                Убийцы времени
                <br />
                <br />
                Приятные задачи, которые не несут практической пользы. К ним
                хочется приступить, уйти от действительно важных задач
                <br />
                Пример: сортировка писем, просмотр сериала
              </>
            }
            onAdd={handleAdd}
          >
            <TasksList
              tasks={store?.['not-urgent-not-important']}
              onMove={handleMove}
              onRemove={handleRemove}
              onEdit={handleEdit}
            />
          </Cell>
        </div>
      </div>
    </DndProvider>
  );
};

export default App;
