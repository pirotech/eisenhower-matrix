import React, { useState } from 'react';
import { useDrag } from 'react-dnd';
import classNames from 'classnames';
import { Cell, Task } from '../types.ts';
import { TaskCardEditor } from '../task-card-editor';
import styles from './task-card.module.scss';

type Props = {
  task: Task;
  onMove?(cell: string, task: Task): void;
  onRemove?(task: Task): void;
  onEdit?(task: Task): void;
};

export const TaskCard = React.memo<Props>((props) => {
  const { task, onMove, onRemove, onEdit } = props;

  const [{ opacity }, dragRef] = useDrag(
    () => ({
      type: 'drag',
      item: task,
      collect: (monitor) => ({
        opacity: monitor.isDragging() ? 0.5 : 1,
      }),
      end: (item, monitor) => {
        const dropResult = monitor.getDropResult<Cell>();
        if (onMove && dropResult?.name) {
          onMove(dropResult?.name, item);
        }
      },
    }),
    [],
  );

  const [copied, setCopied] = useState(false);

  const makeHandleCopy = (text: string) => () => {
    navigator.clipboard.writeText(text).then(() => {
      setCopied(true);
      setTimeout(() => {
        setCopied(false);
      }, 500);
    });
  };

  const [isEdit, setEdit] = useState(false);

  return (
    <div
      ref={dragRef}
      className={styles.taskCard}
      style={{ opacity }}
      onDoubleClick={() => setEdit(true)}
    >
      {isEdit ? (
        <TaskCardEditor
          task={task}
          isEdit={isEdit}
          setEdit={setEdit}
          onEdit={onEdit}
        />
      ) : (
        <>
          {task.label}{' '}
          <div className={styles.buttons}>
            <button
              className={classNames(
                styles.copyButton,
                copied && styles.copyButton_copied,
              )}
              onClick={makeHandleCopy(task.label)}
            >
              <div className={styles.forwardDoc}></div>
              <div className={styles.backwardDoc}></div>
            </button>
            <button className={styles.editButton} onClick={() => setEdit(true)}>
              <div />
            </button>
            <button
              className={styles.removeButton}
              onClick={() => onRemove && onRemove(task)}
            >
              <div className={styles.first} />
              <div className={styles.second} />
            </button>
          </div>
        </>
      )}
    </div>
  );
});

TaskCard.displayName = 'TaskCard';
