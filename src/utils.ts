export const throttle = (callback: Function, delay: number) => {
  let waiting = false;
  return function (...args: any) {
    if (!waiting) {
      callback(...args);
      waiting = true;
      setTimeout(() => {
        waiting = false;
      }, delay);
    }
  };
};
