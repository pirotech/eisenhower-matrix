import React, {
  Dispatch,
  SetStateAction,
  useEffect,
  useRef,
  useState,
} from 'react';
import { Task } from '../types.ts';
import styles from './task-card-editor.module.scss';

type Props = {
  task: Task;
  isEdit: boolean;
  setEdit: Dispatch<SetStateAction<boolean>>;
  onEdit?: (task: Task) => void;
};

export const TaskCardEditor = React.memo<Props>((props) => {
  const { task, isEdit, setEdit, onEdit } = props;

  const inputRef = useRef<HTMLInputElement>(null);
  const [editedLabel, setEditedLabel] = useState(task.label);

  const handleSave = () => {
    onEdit?.({ ...task, label: editedLabel });
    setEdit(false);
  };

  const handleClose = () => {
    setEdit(false);
    setEditedLabel(task.label);
  };

  useEffect(() => {
    const handleKeydown = (event: KeyboardEvent) => {
      if (event.key === 'Escape') {
        handleClose();
      }
      if (event.key === 'Enter') {
        handleSave();
      }
    };
    document.addEventListener('keydown', handleKeydown);
    return () => {
      document.removeEventListener('keydown', handleKeydown);
    };
  }, [handleClose, handleSave]);

  // after edit turn on select all
  useEffect(() => {
    if (isEdit) {
      inputRef.current?.select();
    }
  }, [isEdit]);

  return (
    <>
      <input
        className={styles.input}
        ref={inputRef}
        type={'text'}
        value={editedLabel}
        onChange={(event) => setEditedLabel(event.target.value)}
      />
      <div className={styles.buttons}>
        <button className={styles.acceptButton} onClick={handleSave}>
          <div className={styles.first} />
        </button>
        <button className={styles.closeButton} onClick={handleClose}>
          <div className={styles.first} />
          <div className={styles.second} />
        </button>
      </div>
    </>
  );
});

TaskCardEditor.displayName = 'TaskCardEditor';
