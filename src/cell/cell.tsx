import React, {
  ChangeEventHandler,
  PropsWithChildren,
  ReactNode,
  useState,
} from 'react';
import { v4 } from 'uuid';
import { useDrop } from 'react-dnd';
import classNames from 'classnames';
import { CellsTypes, Styleable, Task } from '../types.ts';
import { useKeyPressed } from '../hooks/useKeyPressed.ts';
import styles from './cell.module.scss';

type Props = PropsWithChildren &
  Styleable & {
    name: CellsTypes;
    hint: ReactNode;
    onAdd(cellType: CellsTypes, task: Task): void;
  };

const HINT_OPENING_KEYS = ['q', 'Q', 'й', 'Й'];

export const Cell = React.memo<Props>((props) => {
  const { children, className, style, name, hint, onAdd } = props;

  const [{ isOver, canDrop }, dropRef] = useDrop(() => ({
    accept: 'drag',
    drop: () => ({ name }),
    collect: (monitor) => ({
      isOver: monitor.isOver(),
      canDrop: monitor.canDrop(),
    }),
  }));

  const [value, setValue] = useState('');

  const handleChange: ChangeEventHandler<HTMLInputElement> = (event) => {
    setValue(event.target.value);
  };

  const handleKeyDown: React.KeyboardEventHandler<HTMLInputElement> = (
    event,
  ) => {
    if (event.key === 'Enter') {
      onAdd(name, { id: v4(), label: value });
      setValue('');
    }
  };

  const hintKeyPressed = useKeyPressed(HINT_OPENING_KEYS);

  return (
    <div
      ref={dropRef}
      className={classNames(
        className,
        styles.cell,
        isOver && canDrop && styles.cell_isOver,
      )}
      style={style}
    >
      <div
        className={classNames(
          styles.cell__content,
          hintKeyPressed && styles.cell__content_blured,
        )}
      >
        {children}
        <div className={styles.cell__input}>
          <input
            type='text'
            value={value}
            onChange={handleChange}
            onKeyDown={handleKeyDown}
          />
        </div>
      </div>
      <div
        className={classNames(
          styles.cell__hint,
          hintKeyPressed && styles.cell__hint_visible,
        )}
      >
        {hint}
      </div>
    </div>
  );
});

Cell.displayName = 'Cell';
