import { useCallback, useEffect, useState } from 'react';
import { throttle } from '../utils.ts';

export const useKeyPressed = (keys: string[]) => {
  const [keyPressed, setKeyPressed] = useState(false);

  const keyPressHandler = useCallback(
    throttle((event: KeyboardEvent) => {
      if (keys.includes(event.key) && event.ctrlKey) {
        setKeyPressed(true);
      }
    }, 50),
    [],
  );

  const keyUpHandler = useCallback((event: KeyboardEvent) => {
    if (keys.includes(event.key) && event.ctrlKey) {
      setKeyPressed(false);
    }
  }, []);

  useEffect(() => {
    document.addEventListener('keydown', keyPressHandler);
    document.addEventListener('keyup', keyUpHandler);

    return () => {
      document.removeEventListener('keypress', keyPressHandler);
      document.removeEventListener('keyup', keyUpHandler);
    };
  }, []);

  return keyPressed;
};
