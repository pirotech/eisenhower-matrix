import { useEffect, useState } from 'react';

/**
 * Хук для стейта, который синхронизируется с local storage
 * @param key ключ для чтения и записи актуального значения в local storage
 * @param defaultValue значение по умолчанию, которое применяется в том случае, если в local storage значения нет
 */
export const useLocalStorageState = <T>(
  key: string,
  defaultValue?: T,
): [T | undefined, React.Dispatch<React.SetStateAction<T | undefined>>] => {
  const [state, setState] = useState<T | undefined>();

  useEffect(() => {
    let localStorageState = localStorage.getItem(key);
    localStorageState = localStorageState
      ? JSON.parse(localStorageState)
      : undefined;
    if (localStorageState) {
      setState(localStorageState as T);
    } else {
      if (defaultValue) {
        setState(defaultValue);
      }
    }
  }, []);

  useEffect(() => {
    if (state) {
      localStorage.setItem(key, JSON.stringify(state));
    }
  }, [state]);

  return [state, setState];
};
