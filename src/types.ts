import React from 'react';

export type Task = {
  id: string;
  label: string;
};

export type Cell = {
  name: string;
};

export type Store = Record<CellsTypes, Task[]>;

export type CellsTypes =
  | 'urgent-important'
  | 'not-urgent-important'
  | 'urgent-not-important'
  | 'not-urgent-not-important';

export type Styleable = Pick<
  React.HTMLAttributes<HTMLDivElement>,
  'className' | 'style'
>;
