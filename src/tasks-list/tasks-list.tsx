import React from 'react';
import { CellsTypes, Task } from '../types.ts';
import { TaskCard } from '../task-card';
import styles from './tasks-list.module.scss';

type Props = {
  tasks: Task[] | undefined;
  onMove: (cellType: CellsTypes, task: Task) => void;
  onRemove: (task: Task) => void;
  onEdit: (task: Task) => void;
};

export const TasksList = React.memo<Props>((props) => {
  const { tasks = [], onMove, onRemove, onEdit } = props;

  if (tasks.length === 0) return null;

  return (
    <div className={styles.tasksList}>
      {tasks?.map((item) => (
        <TaskCard
          key={item.id}
          task={item}
          onMove={onMove}
          onRemove={onRemove}
          onEdit={onEdit}
        />
      ))}
    </div>
  );
});

TasksList.displayName = 'TasksList';
