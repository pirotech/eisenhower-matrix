import React from 'react';
import { Store } from '../types.ts';
import { STORE_PARAM_NAME } from '../constants.ts';
import styles from './share-button.module.scss';

type Props = {
  store: Store | undefined;
};

export const ShareButton = React.memo<Props>((props) => {
  const { store } = props;

  const handleCopy = () => {
    navigator.clipboard.writeText(
      `${location.host}/?${STORE_PARAM_NAME}=${JSON.stringify(store)}`,
    );
  };

  return (
    <button className={styles.shareButton} onClick={handleCopy}>
      <div className={styles.round1}></div>
      <div className={styles.round2}></div>
      <div className={styles.round3}></div>
      <div className={styles.line1}></div>
      <div className={styles.line2}></div>
    </button>
  );
});

ShareButton.displayName = 'ShareButton';
